<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    /**
     * Get all of the post's comments.
     */
    public function likes()
    {
        return $this->morphMany(Like::class, 'like_able');
    }

    /**
     * Get all of the post's comments.
     */
    public function reports()
    {
        return $this->morphMany(Like::class, 'report_able');
    }
}
