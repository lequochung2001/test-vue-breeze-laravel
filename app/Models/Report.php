<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    /**
     * Get all of the owning commentable models.
    */
    public function reportable()
    {
        return $this->morphTo();
    }
}
