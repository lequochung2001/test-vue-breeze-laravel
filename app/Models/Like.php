<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
   /**
     * Get all of the owning commentable models.
    */
    public function like_able()
    {
        return $this->morphTo();
    }
}
