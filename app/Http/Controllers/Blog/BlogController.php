<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Http\Request;
use Inertia\Inertia;

class BlogController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->search;
        $blogs = new Blog();
        if ($search) {
            $blogs = $blogs->where('name', 'like', "%" . $search . "%")
                ->orWhere('meta_descrition', 'like', "%" . $search . "%")
                ->orWhere('content', 'like', "%" . $search . "%");
        }
        $blogs = $blogs->get();
        $props = [
            'blogs' => $blogs
        ];
        return Inertia::render('Blogs/index', $props);
    }

    public function store(Request $request)
    {
        $blog = new Blog();
        $blog->title = $request->title;
        $blog->content = $request->content;
        $blog->meta_description = $request->meta_description;
        $blog->save();
        return redirect(route('blogs.index'));
    }
   
}
