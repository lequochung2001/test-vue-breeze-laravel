<?php

namespace App\Http\Controllers;

use COM;
use Illuminate\Support\Facades\Cache;

class TestController extends Controller
{
    public function index()
    {
        $collection = collect(['taylor', 'abigail', null])->map(function ($name) {
            return strtoupper($name);
        })->reject(function ($name) {
            return empty($name);
        });
        dd($collection);
    }
}
