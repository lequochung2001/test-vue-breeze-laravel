<?php

namespace App\Http\Controllers\Admin\Blogs;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Like;
use Illuminate\Http\Request;
use Inertia\Inertia;

class BlogController extends Controller
{
    public function index()
    {
        $blogs = new Blog();
        $blogs = $blogs->paginate(5);
        $props = [
            'pagination' => [
                'total' => $blogs->total(),
                'per_page' => $blogs->perPage(),
                'current_page' => $blogs->currentPage(),
                'last_page' => $blogs->lastPage(),
                'from' => $blogs->firstItem(),
                'to' => $blogs->lastItem()
            ],
            'data' => $blogs->items(),   
        ];
        return Inertia::render('Admin/Blogs/index', $props);
    }

    public function delete($id)
    {
        $blog = Blog::find($id);
        $blog->delete();
        return redirect(route('admin.blogs.index'));
    }

    public function like($id)
    {
        $blog = Blog::find($id);	
        $like = new Like();
        $blog->likes()->save($like);
        return redirect(route('admin.blogs.index'));
    }

    public function report($id, Request $request)
    {
        $blog = Blog::find($id);	
        $like = new Like();
        $blog->likes()->save($like);
        return redirect(route('admin.blogs.index'));
    }
}
